﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Diagnostics;
using HtmlAgilityPack;

namespace HTMLtoPDF
{
    class Program
    {
        static void Main(string[] args)
        {
            var doc = new HtmlDocument();
            doc.Load("index.html");

            var ul = doc.DocumentNode.Descendants("ul").Single();
            var li = HtmlNode.CreateNode("<li>List number ?</li>");

            ul.AppendChild(li);

            var html = doc.DocumentNode.OuterHtml;
            var pdfBytes = GetPdfByteArray(html);

            File.WriteAllBytes("index.pdf", pdfBytes);
            Process.Start("index.pdf");
        }

        static byte[] GetPdfByteArray(string html)
        {
            var htmlToPdf = new NReco.PdfGenerator.HtmlToPdfConverter();
            return htmlToPdf.GeneratePdf(html);
        }
    }
}
